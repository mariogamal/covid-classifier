package margo.covid.detector.activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;

import margo.covid.detector.R;
import margo.covid.detector.adapter.ItemsAdapter;
import margo.covid.detector.models.Classifier;
import margo.covid.detector.models.ListItem;

public class CovidActivity extends AppCompatActivity {

    public static Classifier.Recognition recognition;

    RecyclerView itemsList;
    CollapsingToolbarLayout toolbarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_covid);
        itemsList = findViewById(R.id.items_list);
        toolbarLayout = findViewById(R.id.collapsing_toolbar);
        loadSOI();
    }

    private void loadSOI() {
        if (recognition.getConfidence() < 0.7)
            initiateSOIView("Stay at home and tke the medicines", getPharmaciesList(), "Minor");
        else if (recognition.getConfidence() < 0.85)
            initiateSOIView("You need to follow up with a doctor", getDoctorsList(), "Moderate");
        else
            initiateSOIView("Please visit a hospital ASAP", getHospitalsList(), "Major");
    }

    private void initiateSOIView(String message, ArrayList<ListItem> items, String soi) {
        ItemsAdapter adapter = new ItemsAdapter(this, items);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        itemsList.setAdapter(adapter);
        toolbarLayout.setTitle("SOI : " + soi);
    }

    private ArrayList<ListItem> getDoctorsList() {
        ArrayList<ListItem> items = new ArrayList<>();
        items.add(new ListItem("Dr Jim Bourne", R.drawable.a, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Jude Jeston", R.drawable.b, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Marie Halpert", R.drawable.c, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Fred Flintstone", R.drawable.d, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.e, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.f, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.g, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.h, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        return items;
    }

    private ArrayList<ListItem> getPharmaciesList() {
        ArrayList<ListItem> items = new ArrayList<>();
        items.add(new ListItem("Dr Jim Bourne", R.drawable.p1, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Jude Jeston", R.drawable.p2, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Marie Halpert", R.drawable.p3, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Fred Flintstone", R.drawable.p4, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.p5, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.p6, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.p7, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.p8, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        return items;
    }

    private ArrayList<ListItem> getHospitalsList() {
        ArrayList<ListItem> items = new ArrayList<>();
        items.add(new ListItem("Dr Jim Bourne", R.drawable.h1, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Jude Jeston", R.drawable.h2, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Marie Halpert", R.drawable.h3, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Fred Flintstone", R.drawable.h4, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.h5, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.h6, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.h7, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        items.add(new ListItem("Dr Margo Sabry", R.drawable.h8, "01211958570", "Assiut, Shrkt Kolta - Yousry Ragheb street "));
        return items;
    }
}